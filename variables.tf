variable "container_name" {
  description = "The mongo container name"
  default = "mongo_container"
  type = string
}
variable "image" {
  description = "The mongo docker image"
  default = "mongo"
  type = string
}
variable "mongo_env" {
  description = "The container env variables"
  type = list
}
variable "mongo_port" {
  description = "The post gres exposed port"
  default = 27017
  type = number
}
variable "host_mongo_path" {
  description = "The mongo host path"
  default = "/tmp/mongo"
  type = string
}
variable "network_name" {
  description = "The network name"
  default = "bridge"
  type = string
}

variable "container_path" {
  description = "container path volume"
  default = "/data/db/"
  type = string
}

variable "create" {
  description = "create network"
  default = true
  type = bool
}
