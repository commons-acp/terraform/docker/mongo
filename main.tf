terraform {
  required_providers {
    docker = {
      "source": "terraform-providers/docker"
    }
  }
}

data "docker_registry_image" "mongo" {
  name = var.image
}
resource "docker_image" "mongo" {
  name          = data.docker_registry_image.mongo.name
  pull_triggers = [data.docker_registry_image.mongo.sha256_digest]
  keep_locally  = true
}
resource "docker_network" "network_bridge" {
  count = var.create ? 1 : 0
  name = var.network_name
  driver = "bridge"
}
resource "docker_container" "mongo" {
  depends_on =[docker_network.network_bridge]
  image = docker_image.mongo.latest
  name  = var.container_name

  volumes {
    host_path = "${var.host_mongo_path}/data/"
    container_path = var.container_path
    read_only = false
  }
  volumes {
    host_path = "${var.host_mongo_path}/conf/mongod.conf/"
    container_path = "/etc/mongod.conf/"
  }

  ports {
    internal = 27017
    external = var.mongo_port
  }

  env = var.mongo_env


  networks_advanced {
    name = var.network_name
  }



}
